#BEGIN {
#		unshift(@INC,'C:\Perl2\Xbase\blib\lib');
#}
use XBase;
use DBI;
use Encode qw(encode decode);

sub write_consol(){
my ($cp_from,$str) = @_;
print Encode::encode('cp866',Encode::decode($cp_from, $str))."\n";
}

&write_consol('utf8',"Утилита выгрузки из dbf в MSSQL.Версия 1.03.Автор:Лютов М.А.(maxim_7\@mail.ru)).");

if (!$ARGV[0]) {
 &write_consol('utf8',"Введите файл dbf : <Утилита.exe> <файл_dbf.dbf> <ODBC mssql>");
 exit;
}

my $file_dbf = $ARGV[0];
my $ind=0; #count for 

my $tab_dbf = new XBase $file_dbf or die XBase->errstr;

@name_dbf = split ("[.]", $ARGV[0]);

&write_consol('utf8',"Читаем структуру таблицы: $file_dbf ");

my @type_field = split("[,]",join(",",$tab_dbf->field_types));
my @name_field = split("[,]",join(",",$tab_dbf->field_names));

my $create_tab = "CREATE TABLE $name_dbf[0] ( " ;   # .join(",",$tab_dbf->field_names).')';

my @byte = $tab_dbf->field_lengths;

my $count_field = 0;
my $n_field=1000; #макс кол-во столбцов таблицы 

for (my $t=0 ; $t<@type_field ; $t++)
{  
   if ( length($name_field[$t])>9 ){ $name_field[$t] = $name_field[$t].'_'.$t ;} # важно ,если у Васи в названии поля более 9 символов , 
                                                                                  #то есть вероятность что появится дубль поля по названию                                                                                   # тк утилита dbatodbf32 отрезает название до 10 символов.    
   if ($type_field[$t]  eq 'N') { $create_tab.=$name_field[$t] ." INT," ; }
   if ($type_field[$t]  eq 'L') { $create_tab.=$name_field[$t] ." INT,";}
   if ($type_field[$t]  eq 'O') {  $create_tab.=$name_field[$t] ." CHAR($byte[$t]),";}
   if ($type_field[$t]  eq 'C') {  $create_tab.=$name_field[$t] ." CHAR($byte[$t]),";}
   if ($type_field[$t]  eq 'D') { $create_tab.=$name_field[$t] ." DATE,";}
 
   $count_field=$t;
   last if ($t > $n_field) # прерываем цикл   по кол-ву полей 1000 ?!.
 }

chop($create_tab);
$create_tab.= " );";

my $export_db= 'sv';

&write_consol('utf8',"\nСоздаем таблицу ".$name_dbf[0]." в MSSql базы $export_db. ");   
#print  $create_tab;

my $usr = 'sa';
my $pwd='12340';
&create_mdb($export_db,$create_tab,$usr,$pwd);
#sleep(3);
&write_consol('utf8',"\nЗаписываем данные в  $name_dbf[0] ...");   

my $str_insert ='';
my $r =0;
    
my $conn_mdb = "DBI:ODBC:".$export_db;  	
my $dbexp=DBI->connect($conn_mdb,$usr,$pwd) or ('conn db:'.$dbexp->errstr());
$dbexp->{LongTruncOk} = 1; 

for (0 .. $tab_dbf->last_record){
   my ($delete, @nom_row) = $tab_dbf->get_record($_);
   $r = $r+1;
   my $param = "'";
   
   for ($i=0;$i<=$count_field ;$i++){
     
     if ($type_field[$i]  eq 'D') 	{
	    if( $nom_row[$i] eq '')
		{ 
		  $param .='1900-01-01';
		  #$param .='NULL';
		}
		else
		{
          $param .= substr ($nom_row[$i],0,4).'-'.substr ($nom_row[$i],4,2).'-'.substr ($nom_row[$i],6,2);
		}
	 }
	 else{ $param .= $nom_row[$i];}
	 
	 if( $i < ($count_field) ) { $param .= "','" ;}
	 else {$param .= "'" ;}
	 
   }
 
   $str_insert = Encode::encode('cp1251',Encode::decode('cp866', "insert into $name_dbf[0]  values ($param);")); 
  	my $exp_res = $dbexp->prepare($str_insert) or &write_consol('cp1251','prepare db:'.$exp_res->errstr());
	$exp_res->execute() or &write_consol('cp1251',"N string=$r : ".$exp_res->errstr());
	$exp_res->finish();
}
$tab_dbf->close;
$dbexp->disconnect();	
&write_consol('utf8',"\nКол-во записей-$r \n");   
sleep(1);

#--------------------------------------------------------------------------
sub create_mdb(){
    
	my($mdb,$SQL_text,$user1,$pass) =@_;
	my $conn_mdb1 = "DBI:ODBC:$mdb";  
	
	my $dbexp1=DBI->connect($conn_mdb1,$user1,$pass) ; #or ('conn db:'.$dbexp1->errstr());
    
	$dbexp1->{LongTruncOk} = 1; 
	my $exp_res1 = $dbexp1->prepare($SQL_text) or &write_consol('cp1251',"\n Msg=$r : ".$exp_res1->errstr());
	
	$exp_res1->execute() or &write_consol('cp1251',"\n Msg=$r : ".$exp_res1->errstr());
	$exp_res1->finish();
	$dbexp1->disconnect();	
 }